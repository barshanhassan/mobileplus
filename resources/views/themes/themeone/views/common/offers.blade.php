<!-- New line required  -->
 <?php $i=0; ?>
 <h5><i class="fa fa-list-ul" ></i>&nbsp;Categories</h5>
@foreach($result['commonContent']['categories'] as $categories_data)
    @php($i++)
    <option value="{{$categories_data->slug}}" @if($categories_data->slug==app('request')->input('category')) selected @endif>{{--{{$categories_data->name}}--}}</option>
    @if(count($categories_data->sub_categories)>0)

        @foreach($categories_data->sub_categories as $sub_categories_data)
            <a class="dropdown-item" href="{{ URL::to('/shop')}}?category={{$sub_categories_data->sub_slug}}"><option value="{{$sub_categories_data->sub_slug}}" @if($sub_categories_data->sub_slug==app('request')->input('category')) selected @endif>{{$sub_categories_data->sub_name}}</option></a>
            @if($i == 3)
                @break;
            @endif
        @endforeach
        @if($i == 3)
            @break;
        @endif
    @endif
@endforeach



{{--
@if(count($result['commonContent']['homeBanners'])>0)
    @foreach(($result['commonContent']['homeBanners']) as $homeBanners)

    @if($homeBanners->type==1)

    <div class="new-product">
	<a title="Banner Image" href="{{ $homeBanners->banners_url}}"><div class="like-bnr" style="background-image: url('{{asset('').$homeBanners->banners_image}}');"></div></a>
    </div>

    @endif

     @if($homeBanners->type==2)

    <a title="Banner Image" href="{{ $homeBanners->banners_url}}"><div class="week-sale-bnr" style="background-image: url('{{asset('').$homeBanners->banners_image}}');"></div></a>

    @endif

    @endforeach
@endif--}}





