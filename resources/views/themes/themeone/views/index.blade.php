
<style>


</style>
@extends('layout')
@section('content')


    <section class="products-content"> @include('common.products') </section>

    <section class="blog-content">
        <div class="container">
            <div class="blog-area">
                <!-- heading -->
                <div class="heading">
                    <h2>@lang('MobilePlus Suppliers')<small class="pull-right"><a href="{{ URL::to('/news')}}">@lang('website.View All')</a></small></h2>
                    <hr>
                </div>

                <div class="row">
                    <div class="col-lg-3 ">
                        <!-- Blog Post -->
                        <div class="blog-post">
                            <article>
                                <div class="">
                                    <div class="blog-overlay">

                                    </div>
                                    <img src="{{asset('')}}/resources/assets/images/news_images/samsung.jpg" alt="">


                                </div>
                                <br>
                                <h4 class="blog-title">
                                    Samsung
                                </h4>
                            </article>
                        </div>

                    </div>
                    <div class="col-lg-3">
                        <!-- Blog Post -->
                        <div class="blog-post">
                            <article>
                                <div class="">
                                    <div class="blog-overlay">
                                    </div>
                                    <img src="{{asset('')}}/resources/assets/images/news_images/gfive.jpg" alt="">
                                </div>
                                <br>
                                <h4 class="blog-title">
                                    G'Five
                                </h4>
                            </article>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <!-- Blog Post -->
                        <div class="blog-post">
                            <article>
                                <div class="">
                                    <div class="blog-overlay">

                                    </div>
                                    <img src="{{asset('')}}/resources/assets/images/news_images/huawei.jpg" alt="">
                                </div>
                                <br>
                                <h4 class="blog-title">
                                    Huawei
                                </h4>
                            </article>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <!-- Blog Post -->
                        <div class="blog-post">
                            <article>
                                <div class="">
                                    <div class="blog-overlay">
                                    </div>
                                    <img src="{{asset('')}}/resources/assets/images/news_images/jyroom.jpg" alt="">
                                </div>
                                <br>
                                <h4 class="blog-title">
                                    Joyroom
                                </h4>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="blog-content">
        <div class="container">
            <div class="blog-area">
                <!-- heading -->
                <div class="heading">
                    <h2>@lang('Our Partners') <small class="pull-right"><a href="{{ URL::to('/#')}}">@lang('')</a></small></h2>
                    <hr>
                </div>
                <div class="row">
                    <div class="blogs blogs-12x">
                        <!-- Blog Post -->
                        <img src="{{asset('')}}/resources/assets/images/site_images/about_partners.jpg" >
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
